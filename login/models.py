# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from friend.models import Expertise, Friend

# Create your models here.
class Human(models.Model):
    npm	 = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    name = models.CharField('Nama', max_length=200)
    email = models.CharField(max_length=200, default="")
    linkedin = models.CharField(max_length=200, default="")
    loggedin =  models.BooleanField(default=False)
    expertise = models.ManyToManyField(Expertise)
    friends = models.ManyToManyField(Friend)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Status(models.Model):
	human = models.ForeignKey(Human)
	date = models.DateTimeField(auto_now_add=True)
	message = models.TextField()
