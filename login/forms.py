from django import forms

class Status_Form(forms.Form):
	error_messages = {
		'required': 'Please fill in the input',
	}
	attrs = {
		'class': 'form-control',
		'rows':4,
		'cols':10,
		'style':'width:85%;',
	}

	status = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, label="")
