from django.conf.urls import url
from .views import index, profile, add_item_to_database, delete_status
from .custom_auth import auth_login, auth_logout


urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^profile/', profile, name='profile'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),


	url(r'^add_status/', add_item_to_database, name='add_status'),
	url(r'^delete/(?P<id>\d+)/', delete_status, name='delete'),
 ]
