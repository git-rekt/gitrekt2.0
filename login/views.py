# Create your views here.
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import Human, Status
from .forms import Status_Form

response = {}

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

# ======================================================================== #
# User Func
# Apa yang dilakukan fungsi INI? #silahkan ganti ini dengan penjelasan kalian
def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('login:profile'))
    else:
        response['author'] = get_data_human(request, 'user_login')
        html = 'login/session/login.html'
        return render(request, html, response)

def profile(request):
    print ("#==> profile")
    if 'user_login' not in request.session.keys():
        response['author'] = get_data_human(request, "user_login")
        html = 'login/session/login.html'
        return render(request, html, response)
    else:
        set_data_for_session(request)
        kode_identitas = get_data_human(request, 'kode_identitas')
        try:
            human = Human.objects.get(npm = kode_identitas)
        except Exception as e:
            human = create_new_human(request)

        response['Status_form'] = Status_Form
        if Status.objects.filter(human=human).count()>0:
            request.session['stat_last'] = Status.objects.filter(human=human).last().message
            response['stat_last'] = request.session['stat_last']
        else:
            request.session['stat_last'] = "No Status yet!"
            response['stat_last'] = request.session['stat_last']

        response['user_login'] = request.session['user_login']
        response['title'] = 'Status'
        response['jumlah_status'] = human.status_set.count()
        list_status(request)
        html = 'login/session/profile.html'
    return render(request, html, response)

def set_data_for_session(request):
    response['author'] = get_data_human(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

    if 'email' in request.session:
        response['name'] = request.session['name']
        response['email'] = request.session['email']
        response['linkedin'] = request.session['linkedin']
        response['picture'] = request.session['picture']
        response['score'] = request.session['score']
    else:
        response['picture'] = "https://www.idlidu.com/Content/images/default.png"
        response['score'] = "1"

def list_status(request):
    #  Implement this function by yourself
    get_data_session(request)
    status = []
    if get_data_human(request, 'user_login'):
        status = get_my_message_from_database(request)
    print(status)
    response['status'] = status

def get_my_message_from_database(request):
    resp = []
    npm = get_data_human(request, 'kode_identitas')
    human = Human.objects.get(npm=npm)
    items = Status.objects.filter(human=human)
    resp = items
    return resp

def get_data_session(request):
    if get_data_human(request, 'user_login'):
        response['author'] = get_data_human(request, 'user_login')

def create_new_human(request):
    name = get_data_human(request, 'user_login')
    npm = get_data_human(request, 'kode_identitas')
    human = Human()
    human.npm = npm
    human.name = name
    human.save()
    return human

def get_data_human(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']
    return data

def add_item_to_database(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST'):
        npm = get_data_human(request, 'kode_identitas')
        human = Human.objects.get(npm = npm)

        status = Status()
        status.message = request.POST['message']
        status.human = human
        status.save()
    return HttpResponseRedirect(reverse('login:profile'))

def delete_status(request, id):
    npm = get_data_human(request,'kode_identitas')
    human = Human.objects.get(npm = npm)
    status = human.status_set.filter(pk=id)
    status.delete()
    return HttpResponseRedirect(reverse('login:profile'))
