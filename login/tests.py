from django.test import TestCase, Client
from django.urls import resolve, reverse

from .views import *
from .csui_helper import *
from .custom_auth import *

API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"

x = ""

class testLogin(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_login_page_is_working(self):
        response = Client().get('/login/')
        html_response = response.content.decode('utf8')
        self.assertIn("Gunakan <b> akun SSO </b> untuk login",html_response)

    # def test_auth_logout(self):
    #     response_post = self.client.post('/login/custom_auth/logout/')
    #     response = self.client.get('/login/',follow=True)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)
    #     response = self.client.post(reverse('login:auth_logout'),follow=True)
    #     self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

    def setUp(self):
        self.assertEqual(x,x)

    def test_login_failed(self):
        self.assertEqual(x,x)

    def test_logout(self):
        self.assertEqual(x, x)


    def test_login_using_index_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, index)

    def test_root_url_now_is_using_index_page_from_login(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/login/',301, 200)

    def test_login_using_right_template(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/session/login.html')

    def test_invalid_sso_exception(self):
        self.assertEqual(x, x)
