[![pipeline status](https://gitlab.com/git-rekt/gitrekt2.0/badges/master/pipeline.svg)](https://gitlab.com/git-rekt/gitrekt2.0/commits/master)
[![coverage report](https://gitlab.com/git-rekt/gitrekt2.0/badges/master/coverage.svg)](https://gitlab.com/git-rekt/gitrekt2.0/commits/master)

Group members:
1. Rasyid Ibrahim S. 	
2. Rizki Maulana R.
3. Aghnia Prawira

Herokuapp: https://gitbig.herokuapp.com
