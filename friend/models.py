from django.db import models

# Create your models here.
class Expertise(models.Model):
    NAME = (
        ('Python', 'Python'),
        ('Java', 'Java'),
        ('C#', 'C#'),
        ('Kotlin', 'Kotlin'),
        ('Erlang', 'Erlang'),
    )

    LEVEL = (
        (1, 'Beginner'),
        (2, 'Intermediate'),
        (3, 'Advanced'),
        (4, 'Expert'),
        (5, 'Legend'),
    )

    COLOR = (
        (1, '#aed3ef'),
        (2, '#75aaff'),
        (3, '#4892f9'),
        (4, '#6eb7ef'),
        (5, '#4784b2'),
    )

    name = models.TextField(choices=NAME)
    level = models.IntegerField(choices=LEVEL)
    color = models.IntegerField(choices=COLOR, default='Blue')

    def __str__(self):
    	return 'Expertise:' + self.name

class Friend(models.Model):
    name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250, unique=True)
    year = models.CharField(max_length=4)
    expertise = models.ManyToManyField(Expertise)
