from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Friend, Expertise

class FriendUnitTest(TestCase):

    def friend_url_is_exist(self):
        response = Client().get('/friend/')
        self.assertEqual(response.status_code, 200)

    def test_friend_using_index_func(self):
        found = resolve('/friend/')
        self.assertEqual(found.func, index)

    def test_friend_using_friend_template(self):
        response = Client().get('/friend/')
        self.assertTemplateUsed(response, 'friend/friend.html')

    def test_model_can_create_new_friend_and_expertise(self):
        #Creating new friend and expertise
        expertise = Expertise(name='Expertise', level=1, color=1)
        expertise1 = Expertise(name='Expertise1', level=1, color=1)
        expertise2 = Expertise(name='Expertise2', level=1, color=1)
        expertise.save()
        expertise1.save()
        expertise2.save()
        new_friend = Friend.objects.create(id=25, name="Friend", npm="111", year="2017")
        new_friend.expertise.add(expertise, expertise1, expertise2)
        #Retrieving all available friend and expertise
        counting_all_available_friend= Friend.objects.all().count()
        counting_all_available_expertise= Expertise.objects.all().count()
        self.assertEqual(counting_all_available_friend,21)
        self.assertEqual(counting_all_available_expertise,11)
