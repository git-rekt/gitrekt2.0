from django.shortcuts import render
from .models import Friend

response = {}
def index(request):
    response['author'] = "gitgud"
    friends = Friend.objects.all()
    response['friends'] = friends
    return render(request, 'friend/friend.html', response)
