from django.contrib import admin
from .models import Expertise, Friend

class MyModelAdmin(admin.ModelAdmin):
    pass
admin.site.register(Expertise, MyModelAdmin)
admin.site.register(Friend, MyModelAdmin)
