from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
import requests
from friend.models import Expertise, Friend
from login.models import Human
import json
# Create your views here.
response = {}

def index(request):
    set_data_for_session(response, request)
    return render(request, 'profiles/profile.html', response)

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['kode_identitas'] = request.session['kode_identitas']
    # response['name'] = request.session['name']
    if 'email' in request.session:
        response['name'] = request.session['name']
        response['email'] = request.session['email']
        response['linkedin'] = request.session['linkedin']
        response['picture'] = request.session['picture']
        response['score'] = request.session['score']
    else:
        response['picture'] = "https://www.idlidu.com/Content/images/default.png"
        response['score'] = "1"

    expertise = get_expertise_from_database(request)
    response['expertise'] = expertise
    response['numOfExpertise'] = expertise.count()

def edit_profile(request):
    set_data_for_session(response, request)
    return render(request, 'profiles/editProfile.html', response)

# def list_status(request):
#     #  Implement this function by yourself
#     get_data_session(request)
#     status = []
#     if get_data_human(request, 'user_login'):
#         status = get_my_message_from_database(request)
#     print(status)
#     response['status'] = status

def get_expertise_from_database(request):
    resp = []
    # npm = get_data_human(request, 'kode_identitas')
    npm = request.session['kode_identitas']
    human = Human.objects.get(npm=npm)
    expertise = human.expertise.all()
    return expertise

# def get_data_session(request):
#     if get_data_human(request, 'user_login'):
#         response['author'] = get_data_human(request, 'user_login')
#
# def get_data_human(request, tipe):
#     data = None
#     if tipe == "user_login" and 'user_login' in request.session:
#         data = request.session['user_login']
#     elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
#         data = request.session['kode_identitas']
#     return data

def delete_expertise(request, id):
    npm = request.session['kode_identitas']
    human = Human.objects.get(npm = npm)
    expertise = human.expertise.filter(pk=id)
    expertise.delete()
    return HttpResponseRedirect(reverse('profile:edit_profile'))

@csrf_exempt
def save_profile_before(request):
    if request.method == 'POST':
        request.session['score'] = request.POST['score']
        response['score'] = request.session['score']

        expertise = json.loads(request.POST['expertise']) #array of expertise
        level = json.loads(request.POST['level']) #array of level
        npm = request.session['kode_identitas']
        human = Human.objects.get(npm=npm)
        human.expertise.all().delete()
        for i in range(len(expertise)):
            human.expertise.add(Expertise.objects.create(name=expertise[i], level=level[i], color=level[i]))

        human.score = request.session['score']
        human.save()

    return JsonResponse({'redirect_url': '/login/'})

@csrf_exempt
def get_profile(request):
    if request.method == 'GET':
        response['score'] = request.session['score']
        return JsonResponse(response)

@csrf_exempt
def save_profile(request):
    if request.method == 'POST':
        request.session['name'] = request.POST['name']
        request.session['email'] = request.POST['email']
        request.session['linkedin'] = request.POST['linkedin']
        request.session['picture'] = request.POST['picture']
        request.session['score'] = request.POST['score']
        response['name'] = request.session['name']
        response['email'] = request.session['email']
        response['linkedin'] = request.session['linkedin']
        response['picture'] = request.session['picture']
        response['score'] = request.session['score']

        expertise = json.loads(request.POST['expertise']) #array of expertise
        level = json.loads(request.POST['level']) #array of level
        npm = request.session['kode_identitas']
        human = Human.objects.get(npm=npm)
        human.expertise.all().delete()
        for i in range(len(expertise)):
            human.expertise.add(Expertise.objects.create(name=expertise[i], level=level[i], color=level[i]))

        human.name = request.session['name']
        human.email = request.session['email']
        human.linkedin = request.session['linkedin']
        human.picture = request.session['picture']
        human.score = request.session['score']
        human.save()

    return JsonResponse({'redirect_url': '/login/'})
