from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, delete_expertise, save_profile, edit_profile, get_profile
from friend.models import Expertise
from login.models import Human

class ProfileUnitTest(TestCase):

    def profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    # def test_profile_edit_using_index_func(self):
    #     found = resolve('/profile/edit')
    #     self.assertEqual(found.func, edit_profile)
    #
    # def test_save_profile_using_index_func(self):
    #     found = resolve('/profile/save')
    #     self.assertEqual(found.func, save_profile)
    #
    # def test_get_profile_using_index_func(self):
    #     found = resolve('/profile/get')
    #     self.assertEqual(found.func, get_profile)

    # def test_profile_using_profile_template(self):
    #     Client().get('/login/')
    #     response = Client().get('/profile/')
    #     self.assertTemplateUsed(response, 'profiles/profile.html')

    # def test_model_can_create_new_friend_and_expertise(self):
    #     #Creating new friend and expertise
    #     expertise = Expertise(id="30", name='Expertise', level=1, color=1)
    #     expertise1 = Expertise(name='Expertise1', level=1, color=1)
    #     expertise2 = Expertise(name='Expertise2', level=1, color=1)
    #     expertise.save()
    #     expertise1.save()
    #     expertise2.save()
    #
    #     human = Human()
    #     human.npm = "122222"
    #     human.name = "Name"
    #     human.expertise.add(expertise, expertise1, expertise2)
    #     human.save()
    #
    #     delete_expertise(request, 30)
    #
    #     self.assertEqual(counting_all_available_expertise,10)
