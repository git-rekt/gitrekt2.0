from django.conf.urls import url
from .views import index, edit_profile, save_profile, delete_expertise, save_profile_before, get_profile
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit/$', edit_profile, name='edit_profile'),
    url(r'^save/$', save_profile, name='save_profile'),
    url(r'^get/$', get_profile, name='get_profile'),
    url(r'^save_before/$', save_profile_before, name='save_profile_before'),
    url(r'^delete_expertise/(?P<id>\d+)/', delete_expertise, name='delete_expertise'),
]
